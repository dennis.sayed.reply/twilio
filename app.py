"""
Script ...

Dennis Sayed - Technology Reply
Data: 20/12/2019
"""

from flask import Flask
from flask import jsonify
from twilio.rest import Client
from twilio.base.exceptions import TwilioRestException
from twilio.twiml.voice_response import VoiceResponse

app = Flask(__name__)

ACCOUNT = "AC5a493690a2cab2f9eb4bc0bac3fc1f42"
TOKEN = "da0ca37ad10738e8aaca9888697e4507"


@app.route('/', methods=['GET'])
def app_main():
    return jsonify({'App': 'Twilio'})


@app.route('/call/<string:number>', methods=['GET'])
def call(number):
    response = VoiceResponse()
    response.say('Benvenuto su twilio')

    try:
        client = Client(ACCOUNT, TOKEN)
        call = client.calls.create(to=number,
                                   from_="+12054420188",
                                   url="http://twimlets.com/holdmusic?Bucket=com.twilio.music.ambient")
        return jsonify({'status': 'chiamata andata a buon file', 'output': call.sid})
    except TwilioRestException as err:
        print(err)
        return jsonify({'status': 'Errore nella chiamata', 'output': call.sid})


@app.route('/message/<string:number>/<string:name>', methods=['GET'])
def message(number, name):
    try:
        client = Client(ACCOUNT, TOKEN)
        message = client.messages.create(to=number, from_="+12054420188",
                                         body="Messaggio automatico inviato da Twilio a %s" % name)
        return jsonify({'output': message.sid})
    except TwilioRestException as err:
        print(err)
        return jsonify({'output': message.sid})


if __name__ == '__main__':
    app.run()
